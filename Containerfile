###
# temporary workaround until crun lands in the upstream repository
###
FROM quay.io/centos/centos:stream9 as builder

RUN dnf install -y 'dnf-command(config-manager)' && \
dnf config-manager --set-enabled crb && \
dnf install -y epel-release epel-next-release

RUN dnf install -y \
make python git gcc automake autoconf libcap-devel \
    systemd-devel yajl-devel libseccomp-devel pkg-config libgcrypt-devel \
    go-md2man glibc-static python3-libmount libtool

RUN mkdir -p /tmp/crun && \
git clone https://github.com/containers/crun.git /tmp/crun

RUN (cd /tmp/crun; ./autogen.sh; ./configure --prefix=/usr; make install)
###
# end of workaround
###

FROM quay.io/centos/centos:stream9

RUN dnf update -y && \
dnf install -y 'dnf-command(config-manager)'

RUN  dnf config-manager --set-enabled crb
RUN dnf copr enable -y mperina/hirte-snapshot centos-stream-9-x86_64 && \
dnf copr enable -y rhcontainerbot/qm centos-stream-9-x86_64

RUN dnf install -y qm bluechi-ctl bluechi podman hostname

COPY files/ /

RUN curl https://raw.githubusercontent.com/containers/qm/main/setup > /usr/share/qm/setup && \
chmod + /usr/share/qm/setup

RUN /usr/share/qm/setup --skip-systemctl --hostname localrootfs

RUN systemctl enable bluechi-controller && \
systemctl enable bluechi-agent

# remove both lines once the previous workaround is not needed anymore
COPY --from=builder /usr/bin/crun /usr/bin/crun
COPY --from=builder /usr/bin/crun /lib/qm/rootfs/usr/bin/crun

ENTRYPOINT ["/sbin/init"]
